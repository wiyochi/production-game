use std::ops::Rem;

use bevy::prelude::*;

const CELL_WIDTH: f32 = 50.;
const CELL_HEIGHT: f32 = 50.;
const GRID_WIDTH: u32 = 10;
const GRID_HEIGHT: u32 = 10;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_event::<MouseWorldMoved>()
        .add_startup_system(setup)
        .add_system(hover_cell)
        .add_system(world_cursor)
        .run();
}

struct MouseWorldMoved {
    position: Vec2,
}

#[derive(Component)]
struct MainCamera;

#[derive(Component)]
struct Cell;

#[derive(Bundle)]
struct CellBundle {
    sprite: SpriteBundle,
    cell: Cell,
}

impl CellBundle {
    fn new(size: Vec2, translation: Vec3) -> Self {
        Self {
            sprite: SpriteBundle {
                sprite: Sprite {
                    color: Color::WHITE,
                    custom_size: Some(size),
                    ..Default::default()
                },
                transform: Transform::from_translation(translation),
                ..Default::default()
            },
            cell: Cell,
        }
    }
}

fn setup(mut commands: Commands) {
    commands.spawn((Camera2dBundle::default(), MainCamera));

    for i in 0..GRID_WIDTH {
        for j in 0..GRID_HEIGHT {
            let x = CELL_WIDTH * (i.rem(GRID_WIDTH) as f32);
            let y = CELL_HEIGHT * (j.rem(GRID_HEIGHT) as f32);

            commands.spawn(CellBundle::new(
                Vec2::new(CELL_WIDTH, CELL_HEIGHT),
                Vec3::new(x, y, 0.),
            ));
        }
    }
}

fn hover_cell(
    mut query: Query<(&Cell, &mut Sprite, &Transform)>,
    mut mouse_evr: EventReader<MouseWorldMoved>,
) {
    for mouse in mouse_evr.iter() {
        for (_, mut sprite, transform) in &mut query {
            let Vec2 { x: mx, y: my } = mouse.position;
            let Vec2 { x: sx, y: sy } = sprite.custom_size.unwrap_or_default() / 2.;
            let Vec3 { x: tx, y: ty, .. } = transform.translation;

            if mx > tx - sx && mx < tx + sx && my > ty - sy && my < ty + sy {
                sprite.color = Color::RED;
            } else {
                sprite.color = Color::WHITE;
            }
        }
    }
}

fn world_cursor(
    mut mouse_world_evw: EventWriter<MouseWorldMoved>,
    mut cursor_events: EventReader<CursorMoved>,
    camera_query: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
) {
    let (camera, camera_transform) = camera_query.single();

    for cursor in cursor_events.iter() {
        if let Some(world_pos) = camera.viewport_to_world_2d(camera_transform, cursor.position) {
            mouse_world_evw.send(MouseWorldMoved {
                position: world_pos,
            });
        }
    }
}
